[![Travis-CI Build Status](https://travis-ci.org/MerrimanLab/merrimanR.svg?branch=master)](https://travis-ci.org/MerrimanLab/merrimanR)
# merrimanR
An R package containing useful functions, rstudio addins, and Rmd templates
